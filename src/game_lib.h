#pragma once

struct Vec3 {
	float x;
	float y;
	float z;

	Vec3 operator+(const Vec3& other) const {
		return { other.x + this->x, other.y + this->y, other.z + this->z };
	}
};

Vec3 translate(const Vec3& inVector, const Vec3& translationVector);

Vec3 rotateAroundZ(const Vec3& inVec, float angleRad);

Vec3 perspectiveProject(const Vec3& input, float hFov, float w, float h);

Vec3 toScreenCoordinates(const Vec3& input, int screenW, int screenH);


