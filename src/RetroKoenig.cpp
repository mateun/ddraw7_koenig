// RetroKoenig.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <ddraw.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <string>

#include "RetroKoenig.h"
#include "game_lib.h"

#define MAX_LOADSTRING 100


double PI = 3.1415926535897932384626433;

// DDRAW7 stuff
LPDIRECTDRAW7 lpdd = NULL;
LPDIRECTDRAWSURFACE7 lpddsPrime = NULL;
LPDIRECTDRAWSURFACE7 lpddsBack = NULL;

LARGE_INTEGER freq;

struct Timer {
	LARGE_INTEGER s;
	LARGE_INTEGER e;
	LONGLONG time;
	void start() {
		QueryPerformanceCounter(&s);
	}

	LONGLONG stop() {
		QueryPerformanceCounter(&e);
		time = (float)((e.QuadPart - s.QuadPart) / (float) freq.QuadPart) * 1000 * 1000;
		return time;
	}
};

// Global Variables:
HINSTANCE hInst;                                // current instance
const char* title = "Koenig v0.0.1";
const char* winclass_name = "koenig_clazz";

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int, HWND& );
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int screen_width = 800;
int screen_height = 600;

// DirectDrawFail Macro
// Calls a directX function and exits on error
#define DDF(m, errorcode) if (FAILED(m)) { exit(errorcode);}


bool createDDraw7Object() {
	HRESULT hr = DirectDrawCreateEx(nullptr, (void**)&lpdd, IID_IDirectDraw7, nullptr);
	return !FAILED(hr);
}
void initDirectDraw(HWND hwnd) {
	if (!createDDraw7Object()) {
		exit(-1);
	}
	lpdd->SetCooperativeLevel(hwnd, DDSCL_EXCLUSIVE | DDSCL_FULLSCREEN | DDSCL_ALLOWREBOOT);
	lpdd->SetDisplayMode(screen_width, screen_height, 32, 0, 0);

	DDSURFACEDESC2 ddsd;
	memset(&ddsd, 0, sizeof(DDSURFACEDESC2));
	ddsd.dwSize = sizeof(DDSURFACEDESC2);
	ddsd.dwFlags = DDSD_CAPS | DDSD_BACKBUFFERCOUNT;
	ddsd.dwBackBufferCount = 1;
	ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_COMPLEX | DDSCAPS_FLIP;
	HRESULT hr = lpdd->CreateSurface(&ddsd, &lpddsPrime, NULL); 
	char* lpBuf;
	if (FAILED(hr)) {
		printf("error: %d\n", hr);
		FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, nullptr, hr, 
			0, (LPTSTR)&lpBuf, 0, NULL);
		exit(2);
	}

	DDSCAPS2 ddscaps2;
	ZeroMemory(&ddscaps2, sizeof(DDSCAPS2));
	ddscaps2.dwCaps = DDSCAPS_BACKBUFFER;
	DDF( lpddsPrime->GetAttachedSurface(&ddscaps2, &lpddsBack), 3)
}

void cleanup() {
	lpddsPrime->Release();
	lpddsPrime = nullptr;
	lpdd->Release();
	lpdd = nullptr;
}

void update() {

}

void setPixel(int x, int y, uint8_t r, uint8_t g, uint8_t b, uint8_t* bb, LONG pitch) {
	int pixelsPerRow = pitch / 1;
	bb[x * 4 + y * pixelsPerRow] = b;
	bb[x * 4 + y * pixelsPerRow + 1] = g;
	bb[x * 4 + y * pixelsPerRow + 2] = r;
	bb[x * 4 + y * pixelsPerRow + 3] = 0;
}

void drawLine(int x1, int y1, int x2, int y2, uint8_t* backBufferPtr, long pitch) {
	int dx = x2 - x1;
	int dy = y2 - y1;

	int dirX = dx < 0 ? -1 : 1;
	int dirY = dy < 0 ? -1 : 1;

	float d = (float)dy / (float) dx;

	float x = x1;
	float y = y1;

	if (abs(d) > 1) {
		// y dominant
		d = (float)dx / (float)dy;
		while (y != y2) {
			setPixel(x, y, 100, 200, 50, backBufferPtr, pitch);
			if (dx < 0) {
				x = x - abs(d) ;
			}
			else {
				x = x + abs(d);
			}
			
			y = y + dirY;
		}
	}
	else
	{
		// x dominant
		while (x != x2) {
			setPixel(x, y, 100, 200, 50, backBufferPtr, pitch);
			if (dy < 0) {
				y = y  - abs(d);
			}
			else {
				y = y + abs(d);
			}
			x = x + dirX;
			
		}
	}

}


float moveH = 0;
float rotZ = 0;


void clearBackBuffer(uint8_t* surfacePointer, LONG pitch) {
	for (int y = 0; y < screen_height; y++) {
		memset(&surfacePointer[y * pitch], 0, pitch);
	}
}

void drawTriangle(Vec3 p1, Vec3 p2, Vec3 p3, uint8_t* surfacePointer, LONG pitch) {
	drawLine(p1.x, p1.y, p2.x, p2.y, surfacePointer, pitch);
	drawLine(p1.x, p1.y, p3.x, p3.y, surfacePointer, pitch);
	drawLine(p2.x, p2.y, p3.x, p3.y, surfacePointer, pitch);
}

void render() {
	DDSURFACEDESC2 ddsd;
	memset(&ddsd, 0, sizeof(DDSURFACEDESC2));
	ddsd.dwSize = sizeof(DDSURFACEDESC2);
	Timer t; 
	t.start();
	HRESULT hr = lpddsBack->Lock(NULL, &ddsd, DDLOCK_NOSYSLOCK | DDLOCK_SURFACEMEMORYPTR | DDLOCK_WAIT, NULL);
	if (FAILED(hr)) {
		printf("oh no! %d\n", hr);
		exit(4);
	}
	uint8_t* surfacePointer = (uint8_t*)ddsd.lpSurface;
	LONG pitch = ddsd.lPitch;
	
	clearBackBuffer(surfacePointer, pitch);

	/*for (int x = 400; x < 799; x++) {
		setPixel(x-300, 100, 255, 2, 25, surfacePointer, pitch);
		setPixel(x, 300, 0, 25, 255, surfacePointer, pitch);
	}

	drawLine(20, 100, 50, 110, surfacePointer, pitch);
	drawLine(20, 110, 80, 140, surfacePointer, pitch);
	drawLine(20, 110, 80, 20, surfacePointer, pitch);
	drawLine(350, 250, 450, 250, surfacePointer, pitch);
	drawLine(450, 250, 400, 350, surfacePointer, pitch);
	drawLine(400, 350, 350, 250, surfacePointer, pitch);
*/
	moveH += 0.009;
	rotZ += 0.008;
	
	Vec3 projDummy1 = { -0.5, 0.5, 2 };
	Vec3 projDummy2 = { 0.5 , 0.5, 2 };
	Vec3 projDummy3 = { 0.5, -0.5, 2 };
	Vec3 projDummy4 = { -0.5, -0.5, 2 };
	Vec3 projDummy5 = { 0.5 , -0.5, 2 };
	Vec3 projDummy6 = { -0.5, 0.5, 2 };

//	projDummy1 = translate(projDummy1, { moveH, 0, 0 });
//	projDummy2 = translate(projDummy2, { moveH, 0, 0 });
//	projDummy3 = translate(projDummy3, { moveH, 0, 0 });

	Vec3 vr1 = rotateAroundZ(projDummy1, moveH);
	Vec3 vr2 = rotateAroundZ(projDummy2, moveH);
	Vec3 vr3 = rotateAroundZ(projDummy3, moveH);

	Vec3 vr4 = rotateAroundZ(projDummy4, rotZ);
	Vec3 vr5 = rotateAroundZ(projDummy5, rotZ);
	Vec3 vr6 = rotateAroundZ(projDummy6, rotZ);

	vr1 = perspectiveProject(vr1, (90 * PI / 180), 1.33, 1);
	vr2 = perspectiveProject(vr2, (90 * PI / 180), 1.33, 1);
	vr3 = perspectiveProject(vr3, (90 * PI / 180), 1.33, 1);

	vr4 = perspectiveProject(vr4, (90 * PI / 180), 1.33, 1);
	vr5 = perspectiveProject(vr5, (90 * PI / 180), 1.33, 1);
	vr6 = perspectiveProject(vr6, (90 * PI / 180), 1.33, 1);

	Vec3 s1 = toScreenCoordinates(vr1, screen_width, screen_height);
	Vec3 s2 = toScreenCoordinates(vr2, screen_width, screen_height);
	Vec3 s3 = toScreenCoordinates(vr3, screen_width, screen_height);

	Vec3 s4 = toScreenCoordinates(vr4, screen_width, screen_height);
	Vec3 s5 = toScreenCoordinates(vr5, screen_width, screen_height);
	Vec3 s6 = toScreenCoordinates(vr6, screen_width, screen_height);


	/*drawLine(s1.x, s1.y, s2.x, s2.y, surfacePointer, pitch);
	drawLine(s1.x, s1.y, s3.x, s3.y, surfacePointer, pitch);
	drawLine(s2.x, s2.y, s3.x, s3.y, surfacePointer, pitch);*/

	drawTriangle(s1, s2, s3, surfacePointer, pitch);
	drawTriangle(s4, s5, s6, surfacePointer, pitch);
		
	setPixel(400, 300, 255, 25, 25, surfacePointer, pitch);



	DDF(lpddsBack->Unlock(NULL), 5)
	LONGLONG diff = t.stop();
	std::string diff_text = "pixelWrite: " + std::to_string(diff) + "\n";
	OutputDebugStringA(diff_text.c_str());
	lpddsPrime->Flip(NULL, DDFLIP_WAIT);
}


int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

   
	HWND wnd;

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow, wnd))
    {
        return FALSE;
    }

  
    MSG msg;

	initDirectDraw(wnd);

	bool gameRunning = true;

	QueryPerformanceFrequency(&freq);
	LARGE_INTEGER start;
	LARGE_INTEGER end;

    // Main message loop:
	while (gameRunning) {
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				gameRunning = false;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		update();
		Timer t1;
		t1.start();
		render();
		LONGLONG diff_in_microseconds = t1.stop();
		std::string diff_text = std::to_string(diff_in_microseconds) + "\n";
		OutputDebugStringA(diff_text.c_str());
	}
    
	cleanup();

    return (int) msg.wParam;
}





//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow, HWND& hwnd)
{

	WNDCLASSA wcex = {};

	//wcex.cbSize = sizeof(WNDCLASSEXA);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	// wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_RETROKOENIG));
	 wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);

	wcex.lpszMenuName = NULL;
	wcex.lpszClassName = winclass_name;
	//wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	RegisterClassA(&wcex);

	hInst = hInstance; // Store instance handle in our global variable

	 hwnd = CreateWindowExA(0, wcex.lpszClassName, title, WS_POPUP  | WS_VISIBLE,
      CW_USEDEFAULT, 0, screen_width, screen_height, nullptr, nullptr, hInstance, nullptr);

   if (!hwnd)
   {
	  DWORD error = GetLastError();
      return FALSE;
   }

   ShowWindow(hwnd, nCmdShow);
   UpdateWindow(hwnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
           
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProcA(hWnd, message, wParam, lParam);
    }
    return 0;
}

